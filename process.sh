#!/bin/bash

if [ ! -f "$1" ]; then
    >&2 echo $0: "$1: No such file"
else
    dos2unix "$1"

    tail -n +2 "$1" | sed -e "s,\"\",',g" -e "s,\",,g" | awk -F";" '
    BEGIN{print "id;name;latitude;longitude;address;type"}
    {printf "%s;%s;%s;%s;%s;%s\n", $3, $4, $5, $6, $7, $8}' > temp.csv

    tail -n +2 "$1" | sed -e "s,\"\",',g" -e "s,\",,g" | awk -F";" '
    BEGIN{print "id;name;latitude;longitude;address;type"}
    {printf "%s;%s;%s;%s;%s;%s\n", $9, $10, $11, $12, $13, $14}' >> temp.csv

    cat temp.csv | grep -v "N/A" | grep -v "^;\+$" | grep -v EMPTY > inter.csv

    #diff temp.csv inter.csv
    rm temp.csv

    sort -n inter.csv | uniq > nodes.csv
    rm inter.csv
fi

if [ ! -f nodes.csv ]; then
    >&2 echo $0: "$1: File not found nodes.csv"
    exit 1
fi

dos2unix nodes.csv

tail -n +2 nodes.csv | awk -F ';' '
BEGIN{
    print "{\"type\": \"FeatureCollection\",\"features\": ["
}{
    printf "%s", ((FNR > 1)? ",\n" : "")
    printf "{\"type\":\"Feature\",\"properties\":{\"id\":\"%s\",\"name\":\"%s\",\"address\":\"%s\",\"type\":\"%s\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[%s,%s]}}", $1, $2, $5, $6, $4, $3
}END{
    printf "\n]}\n"
}' > nodes.geojson

if [ ! -f "$1" ]; then
    >&2 echo $0: "$1: No such file"
    exit
fi

tail -n +2 "$1" | sed -e "s,\"\",',g" -e "s,\",,g" | awk -F";" '
BEGIN{print "start_id;end_id;start_latitude;start_longitude;end_latitude;end_longitude;connection_description;span_description"}
{printf "%s;%s;%s;%s;%s;%s;%s;%s\n", $3, $9, $5, $6, $11, $12, $1, $2}' > temp.csv

cat temp.csv | grep -v "N/A" | grep -v "^;\+$" | grep -v EMPTY  > inter.csv

diff temp.csv inter.csv
rm temp.csv

sort -n inter.csv | uniq > links.csv
rm inter.csv


tail -n +2 links.csv | awk -F ';' '
BEGIN{
    print "{\"type\": \"FeatureCollection\",\"features\": ["
}{
    printf "%s", ((FNR > 1)? ",\n" : "")
    printf "{\"type\":\"Feature\",\"properties\":{\"connection_description\":\"%s\",\"span_description\":\"%s\"},\"geometry\":{\"type\":\"LineString\",\"coordinates\":[[%s,%s],[%s,%s]]}}", $7, $8, $4, $3, $6, $5
}END{
    printf "\n]}\n"
}' > links.geojson
